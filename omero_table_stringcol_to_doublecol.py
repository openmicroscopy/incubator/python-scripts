#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2018 University of Dundee & Open Microscopy Environment.
# All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Script to convert OMERO.tables from StringColumns to Double/LongColumns.

Creates new table, copying columns from old table and
converting StringColumns to Double/LongColumns if all values
are a Double or a Long
Useful for OMERO.parade which doesn't yet support StringColumns.
See https://github.com/ome/omero-parade/issues/47
"""

import omero
from omero.rtypes import rstring
import omero.grid
from omero.gateway import BlitzGateway
from omero.grid import DoubleColumn, LongColumn, StringColumn


USERNAME = "will"
PASSWORD = "ome"
HOST = "localhost"
PORT = 4064

# Plate you want to add the NEW table to:
plate_id = 8067

# The Old table File ID (not annotation ID)
orig_file_id = 113987

conn = BlitzGateway(USERNAME, PASSWORD, host=HOST, port=PORT)
conn.connect()

shared_resources = conn.getSharedResources()
table = shared_resources.openTable(omero.model.OriginalFileI(orig_file_id), conn.SERVICE_OPTS)
headers = table.getHeaders()
number_of_rows = table.getNumberOfRows()
column_data = table.read(range(len(headers)), 0, number_of_rows).columns
table.close()

# Copy columns over, updating column type if String
columns = []
data = []
for column, col_data in zip(headers, column_data):
    name = column.name
    k = column.__class__
    col_values = col_data.values
    first_val = col_values[0]

    print "\n", name, first_val, type(first_val)

    if isinstance(column, StringColumn) and isinstance(first_val, str):
        try:
            col_values = [long(v) for v in col_values]
            k = LongColumn
        except ValueError:
            try:
                col_values = [float(v) for v in col_values]
                k = DoubleColumn
            except ValueError:
                pass
    if k.__name__ == 'StringColumn':
        columns.append(k(name, '', 64, []))
        data.append(k(name, '', 64, col_values))
    else:
        columns.append(k(name, '', []))
        data.append(k(name, '', col_values))

# Make a new table...
new_table = shared_resources.newTable(1, "from table to table")
new_table.initialize(columns)
new_table.addData(data)
new_table.close()

# Link to Plate
orig_file = new_table.getOriginalFile()
fileAnn = omero.model.FileAnnotationI()
fileAnn.ns = rstring('openmicroscopy.org/omero/bulk_annotations')
fileAnn.setFile(omero.model.OriginalFileI(orig_file.id.val, False))
fileAnn = conn.getUpdateService().saveAndReturnObject(fileAnn)
link = omero.model.PlateAnnotationLinkI()
link.setParent(omero.model.PlateI(plate_id, False))
link.setChild(omero.model.FileAnnotationI(fileAnn.id.val, False))

print "save link..."
conn.getUpdateService().saveAndReturnObject(link)

conn.close()
